const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
var s3 = new AWS.S3();

const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c=> {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};


const AccessDataFromS3 = ()=>({
    getWorkLoad : async (Key, Bucket)=>{
      var params = { Bucket, Key };
      let res = await s3.getObject(params).promise();
      return JSON.parse(res.Body.toString('utf-8'));
    }
});

const LogicGeneraSourceRef = (idFinanciera) => ({

  getSourceRef: () => {
    return `${uuidv4()}${idFinanciera}`;

  },

  save: async (sourceRef, documentos) => {
    let result = await ddb.put({
      TableName: 'documentosfinanciera',
      Item: {
        SourceRefDoc: sourceRef,
        IdFinanciera: idFinanciera,
        KeysDocumentos: documentos.map(s=>{return {FolioDoc: s.folio}})
      }
    }).promise();


    return result;
  }

});

exports.handler = async (event) => {
  let accessFromS3 = AccessDataFromS3();
  let file = event.Input.file;
  let bucket = process.env.BUCKET;
  let {Input} = await accessFromS3.getWorkLoad(file, bucket);
  let {documentos, IdFinanciera} = Input;
  let logic = LogicGeneraSourceRef(IdFinanciera);
  let srcRef = logic.getSourceRef();
  await logic.save(srcRef, documentos );
  return {
          output :{Key:file, fecha: new Date()},
          srcRef
  }
};
