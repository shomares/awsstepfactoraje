const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
var s3 = new AWS.S3();

const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c=> {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };

const map = (documento, SourceRefDoc, idFinanciera) => {
        return {
            PutRequest:{
                Item:{
                    FolioDoc: documento.folio,
                    SourceRefDoc: SourceRefDoc,
                    Monto: documento.monto,
                    FechaVencimiento: documento.fechaVencimiento,
                    Aforo: documento.aforo,
                    RfcEpo: documento.rfcEpo,
                    RfcProveedor: documento.rfcProveedor,
                    IdFinanciera: idFinanciera
                }
            }
        }
};

const splitWorkload = (elementos = [], factor, map) => {
    let salida = [], data = [];
    let anterior = 0;
    let i = 1, until = 0;

    while (until < elementos.length - 1) {
        let sFactor  = i * factor;
        until = (sFactor > elementos.length ? elementos.length : sFactor) - 1;
        data = elementos.slice(anterior, until + 1);
        anterior = until + 1;
        salida.push(map(data));
        i++;
    }
    
    return salida;
};

const Registrar = async (documento)=>{
    let documentoKey = documento.map(s=>{
        return {
            PutRequest:{
                Item:{
                    FolioDoc: s.PutRequest.Item.FolioDoc
                }
            }
        }
    });

    var params = {
        RequestItems: {
            documento,
            documentoKey
        }
      };

    await ddb.batchWrite(params).promise();
};

const Validar = async (workload)=>{
    let Keys = workload.map(s=>s.Key);
        var params = {
            RequestItems: {
              'documentoKey': {
                Keys
              }
          }};

        let value = await ddb.batchGet(params).promise();
        let errores = [...value.Responses.documentoKey];
    return errores;
};


const AccessDataFromS3 = ()=>({
    getWorkLoad : async (Key, Bucket)=>{
      var params = { Bucket, Key };
      let res = await s3.getObject(params).promise();
      return JSON.parse(res.Body.toString('utf-8'));
    },

    save : async(Result, Bucket)=>{
      let Key =  `${uuidv4()}.json`;
      var params = { Bucket, Key, Body: JSON.stringify(Result) };
      await s3.putObject(params).promise();
      return {Key, fecha: new Date()};
    }

});

const LogicValidaDocumento = () => ({

    validaDocumento: async (workloads) => {
        console.log('validaDocumento');
        let result = await Promise.all(workloads.map(async workload=>{return await Validar(workload)}));
        let s= result.filter(r=>r.length>0);
        console.log('ok');
        return s;
    },

    save: async (workloads) => {
        await Promise.all(workloads.map(async documento=>{await Registrar(documento)}));
    }

});

exports.handler = async (event) => {
    let logic = LogicValidaDocumento();
    let accesstoS3 = AccessDataFromS3();
    let {srcRef, output} = event.Input;
    let errores = [];
    let bucket = process.env.BUCKET;
    let {Input} = await accesstoS3.getWorkLoad(output.Key, bucket);

    let {documentos, idFinanciera} = Input;
    
    let workload = splitWorkload(documentos,  100, (docs) => { 
        return docs.map(doc=>{
            return {Key:{FolioDoc: doc.folio}, ...doc};
        });
    });

    errores = await logic.validaDocumento(workload);

    console.log(errores);

    
     
    if (errores!==null && errores.length === 0) {
        workload = splitWorkload(documentos,  12, (docs) => { 
            return docs.map(doc=>{
                return  map(doc, srcRef, idFinanciera);
            });
        });
  
        await logic.save(workload);
        return   {  result: true, size:documentos.length, srcRef, output} ;
    }
    else {
        output = await accesstoS3.save({...Input, errores}, bucket);
        return { result: false, output, srcRef } ;
    }
};
