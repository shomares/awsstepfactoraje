const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();


const splitWorkload = (elementos = [], factor, map) => {
    let salida = [], data = [];
    let anterior = 0;
    let i = 1, until = 0;

    while (until < elementos.length - 1) {
        let sFactor = i * factor;
        until = (sFactor > elementos.length ? elementos.length : sFactor) - 1;
        data = elementos.slice(anterior, until + 1);
        anterior = until + 1;
        salida.push(map(data));
        i++;
    }

    return salida;
};

const map = (documento) => {
        return {
            DeleteRequest: {
                Key: {
                    FolioDoc: documento.FolioDoc
                }
            }
        }
 
};

const LogicCancelaDocumento = () => ({
    getDocumentosBySourceRefDoc: async (SourceRefDoc) => {
        var params = {
            TableName: 'documentosfinanciera',
            Key: {
                SourceRefDoc
            }
        };

        let result = await ddb.get(params).promise();
        return result.Item.KeysDocumentos;
    },
    deleteDocs: async (workloads) => {
        for (var i = 0; i < workloads.length; i++) {
            let documentoKey = workloads[i];
            var params = {
                RequestItems: {
                    documentoKey
                }
            };

            await ddb.batchWrite(params).promise();
        }
    }
});

exports.handler = async (event) => {
    let logic = LogicCancelaDocumento();
    let srcRef = event.Input.srcRef;
    let documentos = await logic.getDocumentosBySourceRefDoc(srcRef);

    if (documentos.length > 0) {
        let workloads = splitWorkload(documentos, 25, docs=>{
            return docs.map(s=>map(s));
        });

        await logic.deleteDocs(workloads);
    }

    return {srcRef, result: true};
};
