const AWS = require('aws-sdk');
var ses = new AWS.SES({region: process.env.REGION});
var s3 = new AWS.S3();

const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c=> {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };
  
const LogicEnviarCorreo = ()=>({
    registerTemplate: async ()=>{
        var params = {
            Template: { 
              TemplateName: 'publicacionMasivaT', 
              HtmlPart: '<html><body> <div style="border-left:1px solid gray;border-right:1px solid gray; background-color:#FF6633;padding-bottom:20px;"> <div style="padding:5px;background:#FF6633;"> </div> <div style="background:white;box-shadow:0px 0px 5px black; padding:25px; text-align:justify;"> <p> <font face="open sans"> A continuaci&oacute;n le mostramos los datos del archivo que ha sido publicado: </font> </p> <style> tr th { padding: 5px; box-shadow: 1px 1px 5px gray; } tr td { text-align: right; } </style> <table width="auto" style="margin:0 auto;font-family:open sans;"> <tr> <th align="left">N&uacute;mero de documentos registrados</th> <td align="right" bgcolor="#FF6633" style="color:white;padding:5px;">{{ndocumentos}}</td> </tr> <tr> <th align="left">Monto total publicado</th> <td bgcolor="#FF6633" align="right" style="color:white;padding:5px;">{{montoTotal}}</td> </tr> <tr> <th align="left">Monto menor publicado</th> <td align="right" bgcolor="#FF6633" style="color:white;padding:5px;">{{montoMenor}}</td> </tr> <tr> <th align="left">Monto mayor publicado</th> <td align="right" bgcolor="#FF6633" style="color:white;padding:5px;">{{montoMayor}}</td> </tr> </table> <br> <p> <font face="open sans"> El detalle de dichos documentos los encuentra accediendo a la aplicaci&oacute;n de gesti&oacute;n de documentos, en el men&uacute; <b>>Consultas >Documentos publicados</b> </font> </p> <hr> <font face="open sans" color="gray" size="2pt">Para cualquier duda o comentario, estamos a sus &oacute;rdenes en el &aacute;rea de atenci&oacute;n a clientes en los tel&eacute;fonos 53 26 86 86 y 01 800 504 78 00 Opci&oacute;n 4 Servicios Bancarios y despu&eacute;s Opci&oacute;n 3 Gesti&oacute;n de documentos.</font> </div> <div> <div style="float:left; height:100%; vertical-align:middle"><font face="open sans" color="white" size="2pt"><br />Fecha: {{fechaCompleta}}</font></div> <div style="margin:10px; text-align:right;color:gray;"> <font face="open sans" color="white" size="2pt"> Atentamente<br />Test </font> </div> </div> </div></body></html>',
              SubjectPart: 'CargaMasiva'
            }
          };

      await ses.createTemplate(params).promise();
    },

    templateExists : async ()=>{
        try
        {
            let result = await ses.getTemplate({ TemplateName: 'publicacionMasivaT'}).promise();
            return result !== null;
        }catch(e)
        {
            console.error(e);
            return false;
        }
    },

    send: async (values, to)=>{
       await ses.sendTemplatedEmail({
        Destination: { /* required */
            ToAddresses: [
             to
            ]
          },
          Source: 'shomares@gmail.com', 
          Template: 'publicacionMasivaT',
          TemplateData: JSON.stringify(values),  
       }).promise();
    },

    getSummarize: (documentos, sourceRef)=>{
        let montos = documentos.map(s=>s.monto);
        console.log(montos);
        return {
            ndocumentos: documentos.length,
            montoTotal: montos.reduce((a,b)=> a+b, 0),
            montoMenor : Math.max(...montos),
            montoMayor: Math.min(...montos),
            fechaCompleta: new Date(),
            sourceRef : sourceRef
        }
    }
});

const AccessDataFromS3 = ()=>({
    getWorkLoad : async (Key, Bucket)=>{
      var params = { Bucket, Key };
      let res = await s3.getObject(params).promise();
      return JSON.parse(res.Body.toString('utf-8'));
    },

    save : async(Result, Bucket)=>{
      let Key =  `${uuidv4()}.json`;
      var params = { Bucket, Key, Body: JSON.stringify(Result) };
      await s3.putObject(params).promise();
      return {Key, fecha: new Date()};
    }

});


class ServiceCorreoDoesntWorkOut extends Error{
    constructor(message)
    {
        super(JSON.stringify(message));
        this.name = this.constructor.name;
        if (typeof Error.captureStackTrace === 'function'){
        Error.captureStackTrace(this, this.constructor);
          } else { 
            this.stack = (new Error(message)).stack; 
          }
     
    }
}

exports.handler = async (event) => {
    let logic = LogicEnviarCorreo();
    let accesstoS3  = AccessDataFromS3();
    let {srcRef, output} = event.Input;
    let bucket = process.env.BUCKET;

    let {Input} = await accesstoS3.getWorkLoad(output.Key,bucket);
    let {documentos, email} = Input;


    let summarize = logic.getSummarize(documentos, srcRef);
    console.log(summarize);
    
    try {
        if(!(await logic.templateExists()))
        {
            await logic.registerTemplate();
        }

        await logic.send(summarize, email);
        return summarize;
    } catch (e) {
        throw new ServiceCorreoDoesntWorkOut({error:e, Input:event.Input});
    }
 
};
