{
    "Comment": "Carga masiva de documento a partir de AWS Step Functions",
    "StartAt": "GeneraSourceRef",
    "States": {
      "GeneraSourceRef": {
        "Comment": "Genera el sourceref para la carga masiva",
        "Resource": "arn:aws:states:::lambda:invoke",
        "Type": "Task",
        "Parameters": {
            "FunctionName": "${GenerarSourceRefDoc}",
            "Payload": {
                        "Input.$": "$.value"
            }
        },
        "Next": "ValidarDocumento"
      },
      "ValidarDocumento": {
        "Comment": "Valida los documentos",
        "Type": "Task", 
        "Resource": "arn:aws:states:::lambda:invoke",
        "Parameters": {
            "FunctionName": "${ValidarDocumento}",
            "Payload" : {
                "Input.$": "$.Payload"
            }
        },
        "Next": "EvaluarResultado"
      },
      "EvaluarResultado": {
        "Type": "Choice",
        "Choices": [
                    {
                        "Not": {
                            "Variable": "$.Payload.result",
                            "BooleanEquals": true
                        },
                        "Next": "BorrarCargaMasiva"
                    },
                    {
                        "Variable": "$.Payload.result",
                        "BooleanEquals": true,
                        "Next": "Notificar"
                    }
                ]
        },
        "Notificar": {
            "Type": "Parallel",
            "Branches" : [
                {
                    "StartAt": "NotificarCargaAdministrador",
                    "States": {
                        "NotificarCargaAdministrador": {
                            "Type": "Task",
                            "Resource": "arn:aws:states:::sns:publish",
                            "Parameters": {
                              "Message": {
                                "Input": "Se ha registrado una carga masiva de documento correctamente"
                              },
                              "TopicArn": "${PublicacionMasiva}"
                            },
                            "End": true
                          }
                    }
                },
                {
                    "StartAt": "NotficarCargaEpo",
                    "States":{
                        "NotficarCargaEpo":{
                            "Comment": "Notifica a la EPO de carga masiva",
                            "Type": "Task",
                            "Resource": "arn:aws:states:::lambda:invoke",
                            "Parameters": {
                                "FunctionName": "${EnviarCorreo}",
                                "Payload": {
                                    "Input.$" : "$.Payload"
                                }
                            },
                            "Retry":[ {
                                    "ErrorEquals": ["Error"],
                                    "IntervalSeconds" : 2,
                                    "BackoffRate": 2.0,
                                    "MaxAttempts": 2
                                    }
                            ],
                            "End": true
                        }
                    }
                }
            ],
            "Catch":[
                {  
                    "ErrorEquals": ["States.ALL"] ,
                    "Next": "FormatearError"
                }
            ],
            "Next" : "Finalizar" 
        },
        "FormatearError":{
            "Comment": "Formatea el mensaje de error",
            "Type": "Task", 
            "Resource": "arn:aws:states:::lambda:invoke",
            "Parameters": {
                "FunctionName": "${FormatoMensajeError}",
                "Payload" : {
                    "Input.$": "$.Payload"
                }
            },
            "Next": "BorrarKeysDocumento"
        },
        "BorrarKeysDocumento":{
            "Comment": "Borra las llaves del documento",
            "Type": "Task", 
            "Resource": "arn:aws:states:::lambda:invoke",
            "Parameters": {
                "FunctionName": "${CancelaKeyDocumento}",
                "Payload" : {
                    "Input.$": "$.Payload"
                }
            },
            "Next": "BorrarCargaMasiva"
        },
      "BorrarCargaMasiva": {
                "Comment": "Borra la carga masiva",
                "Type": "Task", 
                "Resource": "arn:aws:states:::lambda:invoke",
                "Parameters": {
                    "FunctionName": "${CancelarDocumento}",
                    "Payload" : {
                        "Input.$": "$.Payload"
                    }
                },
                "Next": "Finalizar"
            },
      "Finalizar": {
           "Type": "Pass",
            "End": true
      }
        }
    }
    
    