resource "aws_dynamodb_table" "documento" {
  name = "documento"
  hash_key = "FolioDoc"
  range_key = "SourceRefDoc"
  attribute {
    name = "SourceRefDoc"
    type = "S"
  }
  attribute {
    name = "FolioDoc"
    type = "S"
  }
  write_capacity = 2
  read_capacity = 10
}

resource "aws_dynamodb_table" "documentoKey" {
    name = "documentoKey"
    hash_key = "FolioDoc"
    attribute {
    name = "FolioDoc"
    type = "S"
  }
     write_capacity = 2
     read_capacity = 10
 
}

resource "aws_dynamodb_table" "documentoFinanciera" {
  name = "documentoFinanciera"
  hash_key = "SourceRefDoc"
   attribute {
    name = "SourceRefDoc"
    type = "S"
   }
   write_capacity = 2
   read_capacity = 10
}


