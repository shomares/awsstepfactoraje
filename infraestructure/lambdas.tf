///Files ZIP-----------------------------------------
data "archive_file" "cancelarDocumentozip" {
    type = "zip"
    source_dir  = "../lambdas/CancelarDocumento"
    output_path = "CancelarDocumento.zip"
}

data "archive_file" "cancelarKeyDocumento" {
    type = "zip"
    source_dir = "../lambdas/CancelarKeyDocumento"
    output_path = "CancelarKeyDocumento.zip"
}

data "archive_file" "enviarCorreo" {
    type = "zip"
    source_dir = "../lambdas/EnviarCorreo"
    output_path = "EnviarCorreo.zip"
}


data "archive_file" "formatoMensajeError" {
    type = "zip"
    source_dir = "../lambdas/FormatoMensajeError"
    output_path = "FormatoMensajeError.zip"
}

data "archive_file" "generarSourceRefDoc" {
    type = "zip"
    source_dir = "../lambdas/GenerarSourceRefDoc"
    output_path = "GenerarSourceRefDoc.zip"
}

data "archive_file" "validarDocumento" {
    type = "zip"
    source_dir = "../lambdas/ValidarDocumento"
    output_path = "ValidarDocumento.zip"
}

//Lambdas function--------------------------------

resource "aws_lambda_function" "CancelarDocumento" {
    function_name = "CancelarDocumento"
    role = "${aws_iam_role.lambdaRoleDatabase.arn}"
    filename = "${data.archive_file.cancelarDocumentozip.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
}

resource "aws_lambda_function" "CancelarKeyDocumento" {
    function_name = "CancelarKeyDocumento"
    role = "${aws_iam_role.lambdaRoleDatabase.arn}"
    filename = "${data.archive_file.cancelarKeyDocumento.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
    environment {
         variables = {
            BUCKET = "${aws_s3_bucket.cargaMasivaTerraform0001.bucket}"
         }
    }
}

resource "aws_lambda_function" "EnviarCorreo" {
    function_name = "EnviarCorreo"
    role = "${aws_iam_role.lamdaRoleSimpleServiceMail.arn}"
    filename = "${data.archive_file.enviarCorreo.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
    environment {
    variables = {
      BUCKET = "${aws_s3_bucket.cargaMasivaTerraform0001.bucket}"
      REGION = "${var.region}"
    }
  }
}

resource "aws_lambda_function" "FormatoMensajeError" {
    function_name = "FormatoMensajeError"
    role = "${aws_iam_role.lambdaRoleDatabase.arn}"
    filename = "${data.archive_file.formatoMensajeError.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
   
}

resource "aws_lambda_function" "GenerarSourceRefDoc" {
    function_name = "GenerarSourceRefDoc"
    role = "${aws_iam_role.lamdaRoleSimpleServiceMail.arn}"
    filename = "${data.archive_file.generarSourceRefDoc.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
    environment {
    variables = {
      BUCKET = "${aws_s3_bucket.cargaMasivaTerraform0001.bucket}"
    }
  } 
}

resource "aws_lambda_function" "ValidaDocumento" {
    function_name = "ValidarDocumento"
    role = "${aws_iam_role.lambdaRoleDatabase.arn}"
    filename = "${data.archive_file.validarDocumento.output_path}"
    handler = "index.handler"
    runtime = "nodejs12.x"
    environment {
    variables = {
      BUCKET = "${aws_s3_bucket.cargaMasivaTerraform0001.bucket}"
    }
  } 
}
