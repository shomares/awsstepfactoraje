data "template_file" "policyDatabase" {
  template = "${file("policies/accesstoDynamoDB.tpl")}"
  vars = {
    documentoKey = "${aws_dynamodb_table.documentoKey.arn}"
    documentosfinanciera = "${aws_dynamodb_table.documentoFinanciera.arn}"
    documento = "${aws_dynamodb_table.documento.arn}"
  }
}

data "template_file" "policyStepFunctions" {
  template = "${file("policies/policyStepFunctions.tpl")}"
  vars = {
      ValidarDocumento = "${aws_lambda_function.ValidaDocumento.arn}"
      GenerarSourceRefDoc = "${aws_lambda_function.GenerarSourceRefDoc.arn}"
      CancelarDocumento = "${aws_lambda_function.CancelarDocumento.arn}"
      EnviarCorreo = "${aws_lambda_function.EnviarCorreo.arn}"
      FormatoMensajeError = "${aws_lambda_function.FormatoMensajeError.arn}"
      CancelaKeyDocumento = "${aws_lambda_function.CancelarKeyDocumento.arn}"
  }
}

data "template_file" "policyStepFunctionsSNS" {
  template = "${file("policies/accesstoSNS.tpl")}"
  vars = {
      PublicacionMasiva = "${aws_sns_topic.PublicacionMasivaF.arn}"
      
  }
}



data "template_file" "definitionStepFunction" {
  template = "${file("../awssteps/publicacionMasiva.tpl")}"
  vars = {
      ValidarDocumento = "${aws_lambda_function.ValidaDocumento.arn}"
      GenerarSourceRefDoc = "${aws_lambda_function.GenerarSourceRefDoc.arn}"
      CancelarDocumento = "${aws_lambda_function.CancelarDocumento.arn}"
      EnviarCorreo = "${aws_lambda_function.EnviarCorreo.arn}"
      FormatoMensajeError = "${aws_lambda_function.FormatoMensajeError.arn}"
      CancelaKeyDocumento = "${aws_lambda_function.CancelarKeyDocumento.arn}"
      PublicacionMasiva = "${aws_sns_topic.PublicacionMasivaF.arn}"


  }
}