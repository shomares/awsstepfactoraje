resource "aws_sfn_state_machine" "PublicacionMasivaSTS" {
    name = "PublicacionMasiva"
    role_arn = "${aws_iam_role.CallLamdaFromAwsStepFunction.arn}"
    definition = "${data.template_file.definitionStepFunction.rendered}"
  
}
