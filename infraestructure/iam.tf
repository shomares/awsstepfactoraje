///Policies------------------------

resource "aws_iam_policy" "accesstoS3" {
    depends_on = ["aws_s3_bucket.cargaMasivaTerraform0001"]
    name = "accesstoS3"
    policy = "${file("policies/accesstoS3.json")}"
}

resource "aws_iam_policy" "accesstoDynamoDB" {
    depends_on = ["aws_dynamodb_table.documento", "aws_dynamodb_table.documentoFinanciera", "aws_dynamodb_table.documentoKey"]
     name = "accesstoDynamoDB"
     policy = "${data.template_file.policyDatabase.rendered}"
  
}

resource "aws_iam_policy" "accesstoSimpleEmailService" {
  name = "accesstoSimpleEmailService"
  policy = "${file("policies/accesstoSimpleEmailService.json")}"
}

resource "aws_iam_policy" "awsStepFunctionsCall" {
    name = "awsStepFunctionsCall"
    policy = "${data.template_file.policyDatabase.rendered}"

}

resource "aws_iam_policy" "awsStepFunctionSNS" {
    name = "awsStepFunctionSNS"
      policy = "${data.template_file.policyStepFunctionsSNS.rendered}"

  
}




//Roles----------------------------------------------------
resource "aws_iam_role" "lambdaRoleDatabase" {
    name = "lambdaAccessToS3andDynamo"
    assume_role_policy  = "${file("policies/lambdaRole.json")}"
}

resource "aws_iam_role" "lamdaRoleSimpleServiceMail" {
    name = "lamdaRoleSimpleServiceMail"
    assume_role_policy  =  "${file("policies/lambdaRole.json")}"
}

resource "aws_iam_role" "CallLamdaFromAwsStepFunction" {
    name = "callLamdaFromAwsStepFunction"
    assume_role_policy = "${file("policies/lambdaStep.json")}"
}


///Attachment Lambda Database -------------------------------------------------
resource "aws_iam_role_policy_attachment" "attachmentS3RoleDatabase" {
    role = "${aws_iam_role.lambdaRoleDatabase.name}"
    policy_arn = "${aws_iam_policy.accesstoS3.arn}"
}

resource "aws_iam_role_policy_attachment" "attachmentDynamoRoleDatabase" {
    role = "${aws_iam_role.lambdaRoleDatabase.name}"
    policy_arn = "${aws_iam_policy.accesstoDynamoDB.arn}"
}

resource "aws_iam_role_policy_attachment" "attachmentLambdaRoleDatabase" {
   role = "${aws_iam_role.lambdaRoleDatabase.name}"
   policy_arn = "${var.AWSLambdaBasicExecutionRole}"
}



///Attachment Lambda SES -------------------------------------------------
resource "aws_iam_role_policy_attachment" "attachmentS3RoleMail" {
    role = "${aws_iam_role.lamdaRoleSimpleServiceMail.name}"
    policy_arn = "${aws_iam_policy.accesstoS3.arn}"
}

resource "aws_iam_role_policy_attachment" "attachmentDynamoRoleMail" {
    role = "${aws_iam_role.lamdaRoleSimpleServiceMail.name}"
    policy_arn = "${aws_iam_policy.accesstoDynamoDB.arn}"
}

resource "aws_iam_role_policy_attachment" "attachmentLambdaRoleMail" {
   role = "${aws_iam_role.lamdaRoleSimpleServiceMail.name}"
   policy_arn = "${var.AWSLambdaBasicExecutionRole}"
}


resource "aws_iam_role_policy_attachment" "attachmentSMSRoleMail" {
   role = "${aws_iam_role.lamdaRoleSimpleServiceMail.name}"
   policy_arn = "${aws_iam_policy.accesstoSimpleEmailService.arn}"
}

///Atachment Step Functions--------------------------------
resource "aws_iam_role_policy_attachment" "attachmentawsStepFunctionsCall" {
    role = "${aws_iam_role.CallLamdaFromAwsStepFunction.name}"
    policy_arn = "${aws_iam_policy.awsStepFunctionsCall.arn}"
  
}

resource "aws_iam_role_policy_attachment" "attachmentawsStepFunctionsCallSNS" {
  role = "${aws_iam_role.CallLamdaFromAwsStepFunction.name}"
  policy_arn = "${aws_iam_policy.awsStepFunctionSNS.arn}"
  
}


    




